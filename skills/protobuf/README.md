# Protobuf 编解码

参考： https://gitee.com/infraboard/go-course/blob/master/day15/protobuf.md


## IDE（Vscode）

+ vscode-proto3


## 编译

```sh 
   protoc -I=. --go_out=. hello.proto
```