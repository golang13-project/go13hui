package unittest_test

import (
	"fmt"
	"os"
	"strconv"
	"testing"

	"gitlab.com/golang13-project/go13hui/skills/unittest"
)

// 这个单元测试需要读取外部变量?
// 这里的ARG1/ARG2如果传递给单元测试
// D:\Program Files\Go\bin\go.exe test -timeout 30s -run ^TestSum$ gitlab.com/golang13-project/go13hui/skills/unittest -v -count=1
// 当我们点击 debug test时 怎么注入自定义环境变量
//直接点14行前面的勾就可以运行debug test ，而且输出到result
func TestSum(t *testing.T) {
	//  文件有相对路由和绝对路径
	// 环境变量读取和使用
	a1 := os.Getenv("ARGV1")
	a2 := os.Getenv("ARGV2")
	a1I, _ := strconv.Atoi(a1)
	a2I, _ := strconv.Atoi(a2)
	fmt.Println(a1I, a2I)
	t.Log(unittest.Sum(a1I, a2I))
	// t.Log(unittest.Sum(4, 5))
}
