
var a = (fn) => {
    // a 执行完成后，调用函数fn
    // setTimeout(callback,1000) // 1秒后调用callback
    // 失败就直接返回
    console.log('网络请求a')
    //成功就执行fn
    // fn()
}


var b = (fn) => {
        // b 执行完成后，调用函数fn
     // 失败就直接返回
     console.log('网络请求b')
     //成功就执行fn
    //  fn()
}


var c = () => {
    // c 执行完成后，调用函数fn
     // 失败就直接返回
     console.log('网络请求c')
     //成功就执行fn
    //  fn()
}

// 同时调用，a,b,c 同时调用
//  url1，url2，url3 相当于同时打开3个网站
a(()=> {
    console.log('网络请求a完成')
    b(() =>{
        console.log('网络请求b完成')
        c(() =>{
            console.log('网络请求c完成')
        }
            
        )

    })
})


// Promise , 调用blog的 blog list api
// 成功：[item...]
// 失败: 提示用户
function BlogList(success,failed) {
    // io 网络请求
    //通过http status code
    
    // 成功 调用success处理返回数据
    var items  = []
    success(items)  

    // 失败
    var  err = new Error('请求失败')
    failed(err)
    
    // 为了规范编程， 发展了promise对象，把函数构造成一个promise对象
}

var po = new Promise(BlogList)
po.then((data) => {
    console.log(data)
}).catch((err) =>{
    console.log(err)
}).finally((err) => {
    console.log("finnal")
})

// async await
// await 是等待promise对象的返回
async function f1(){
    try {
        const p = new Promise(BlogList)
        var resp = await p
        console.log("x",resp)
    }catch (error) {
        console.log(error)
    }
  
}
f1()

// async function f2(param){
//     return param + '.' + "f2"
// }

// async function f3(param){
//     return param + '.' + "f3"
// }

// var resp1 = await f1()
// var resp2 = await f2(resp1)
// var resp3 = await f3(resp2)
// console.log(resp3)

// 调用api的具体例子