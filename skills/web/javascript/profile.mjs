//profile.js

// export var firstName = 'micha';
// export var lastName = 'Jackson';
// export var year = '1988';

 var firstName = 'micha';
 var lastName = 'Jackson';
var year = '1988';


// //通过把变量组装成map导出
// export{ firstName,lastName,year}


// export var MYAPP = { firstName,lastName,year}
// MYAPP.name = 'myapp';
// MYAPP.version = '1.0'


// default = {firstName: firstName,lastName:lastName,year:year}
export default {
    firstName,
    lastName,
    year
}