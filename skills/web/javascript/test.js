
// js 变量作用域
{
var a = 'test'
{
let a = 10
console.log(a)
}

console.log(a)
}

// 结构赋值
// a1 = [1,2,3]
// x = a1[0]
// y = a1[1]
// z = a1[2]
// 变量数组==map  value数组
[x,y,z] = [1,2,3]
console.log(x,y,z)

//对象的解构赋值
b1= {name:'wangwu',age:10}
var {name,age} = b1
name = b1.name
age = b1.age
console.log(name,age)
console.log({name,age})

// go fmt

console.log('你好！'+ name)

// b2 = 'test'
// try {
//     a3 = null;
//     console.log(a3[0]);
// } catch (error){
//     console.log(error);
// } finally {
//     console.log('asdf')
// }

//手动抛出异常
try{
    // 1. new exception
    // Error 是一种类型
     var e  = new Error('定义异常')
    //2.再抛出 exception
    throw e
} catch (error){
    console.log(error)
}

// 函数
function abs(x){
    if (x >= 0){
        return x;
    }else {
        return -x;
    }
}

console.log(abs(-10))

// 方法： 绑定在对象上的函数

var person = {name :'lisi',age:19}
// person.great = function() {
//     //this 指代当前指定的对象
//     console.log(`nihao woshi${this.name}`)
// }
// 这里的great就是方法
// person.great = {} => { console.log(`nihao woshi${this.name}`)}
// person.great()

// 等价于下面的函数
function sqr(x){
    return x*x;
}
// x = x*x
// var sq = x => x*x
var sq = (x) =>{return x*x}
console.log(sq(10))

// js 导入与导出
// import {firstName,lastName,year} from './profile.mjs'
// console.log(firstName,lastName,year)

// FOR 
arr1 = [1,2,3]
for (var x of arr1){
    console.log(x)
}
// for of 遍历map
var  o = {
    name: 'jack',
    age: 20,
    city: 'beijing'
};
// Object.keys, Array/Object/Error
// 工具方法
for (var x of Object.keys(o)){
    console.log(x);
}

// 使用array对象提供一个迭代器，传递一个函数作为参数
// fn = element => {}
// 不能中断循环
arr1.forEach(element => {
    console.log(element)
});