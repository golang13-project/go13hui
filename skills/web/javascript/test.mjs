
//map解构赋值
// import {firstName as profilefirstName,lastName,year} from './profile.mjs'

// import { MYAPP } from './profile.mjs'

import pkg from './profile.mjs'


// 能不能通过包名来引入包里面的变量

// var firstName = 'xxxx'
// 变量冲突，可以对导入的变量进行重命名
// console.log(firstName,lastName,year)
// console.log(MYAPP.firstName,MYAPP.lastName,MYAPP.year)

console.log(pkg.firstName,pkg.lastName,pkg.year)