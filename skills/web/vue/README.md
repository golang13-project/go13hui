# 初始化一个vue项目
```sh
    npm create vue@latest
```




快速上手
我们先快速初始化一个vue3的项目, 让我们对vue有一个感性的认识

安装
vue提供了一套脚手架用于快速初始化一个vue3的项目

$npm init vue@latest

Need to install the following packages:
  create-vue@3.2.2
Ok to proceed? (y) y

Vue.js - The Progressive JavaScript Framework

✔ Project name: … vue3_example
✔ Add TypeScript? … No / Yes
✔ Add JSX Support? … No / Yes
✔ Add Vue Router for Single Page Application development? … No / Yes
✔ Add Pinia for state management? … No / Yes
✔ Add Vitest for Unit Testing? … No / Yes
✔ Add Cypress for End-to-End testing? … No / Yes
✔ Add ESLint for code quality? … No / Yes
✔ Add Prettier for code formatting? … No / Yes

Scaffolding project in /Users/yumaojun/Workspace/Golang/go-course/extra/devcloud/vue3_example...

Done. Now run:

  cd vue3_example
  npm install
  npm run lint
  npm run dev


参考：
https://gitee.com/infraboard/go-course/blob/master/day20/vue3.md

## 项目架构

[](./docs/arch.drawio)
