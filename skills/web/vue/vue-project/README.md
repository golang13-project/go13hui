# vue-project

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur).

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```







定义 API获取列表数据
### 关键字搜索
1. BlogList(page_size,page_number,...)

后端：
+ keywords 关键字查询条件
+ http 请求解析  该用户参数
+ BlogList接口  实现参数的处理

前端：
+ request.keywords  <---- > a-input
+ 用户回车时 触发blog list api调用


###  文章的创建与编辑

独立使用一个页面： CreateOrUpdateBlog。vue，点击跳转

