import { createApp } from 'vue'
import App from './App.vue'

// 注册全局组件
//import Component from './Components/'
createApp(App).mount('#app')
