package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/golang13-project/go13hui/vblog/apps/blog"
	"gitlab.com/golang13-project/go13hui/vblog/ioc"
	"gitlab.com/golang13-project/go13hui/vblog/middleware"
)

func init() {
	ioc.Api().Registry(blog.AppName, &blogApiHandler{})
}

type blogApiHandler struct {
	svc blog.Service
}

func (i *blogApiHandler) Init() error {
	i.svc = ioc.Controller().Get(blog.AppName).(blog.Service)
	return nil
}

func (i *blogApiHandler) Destory() error {
	return nil
}

// 让ioc帮我们完成接口的路由注册 ioc.GinApi
//
//	type GinApi interface {
//		// 基础约束
//		Object
//		// 额外约束
//		// ioc.Api().Get(token.AppName).(*api.TokenApiHandler).Registry(rr)
//		Registry(rr gin.IRouter)
//	}

func (i *blogApiHandler) Registry(rr gin.IRouter) {
	r := rr.Group(blog.AppName)

	// 普通接口，允许访客使用， 不需要权限
	r.GET("/", i.QueryBlog)
	r.GET("/:id", i.DescribeBlog)

	// 整个模块后面的请求  都需求认证
	r.Use(middleware.Auth)

	// 后面管理的接口   需要权限
	r.POST("/", i.CreateBlog)
	r.PATCH("/:id", i.PatchBlog)
	r.PUT("/:id", i.UpdateBlog)
	r.DELETE("/:id", i.DeleteBlog)

}
