package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/golang13-project/go13hui/vblog/apps/blog"
	"gitlab.com/golang13-project/go13hui/vblog/common"
	"gitlab.com/golang13-project/go13hui/vblog/response"
)

// 创建博客: POST /vblogs/api/v1/blogs
func (h *blogApiHandler) CreateBlog(c *gin.Context) {
	req := blog.NewCreateBlogRequest()
	if err := c.BindJSON(req); err != nil {
		response.Failed(c, err)
		return
	}
	ins, err := h.svc.CreateBlog(c.Request.Context(), req)
	if err != nil {
		response.Failed(c, err)
		return
	}
	response.Success(c, ins)
}

// 修改博客(部分)： PATCH /vblogs/api/v1/blogs/:id
// /vblogs/api/v1/blogs/10 --> id = 10
// c.Param("id")  获取路径变量的值

func (h *blogApiHandler) PatchBlog(c *gin.Context) {
	req := blog.NewUpdateBlogRequest(c.Param("id"))
	req.UpdateMode = common.UPDATE_MODE_PATCH
	// 用户传递的数据
	if err := c.BindJSON(req.CreateBlogRequest); err != nil {
		response.Failed(c, err)
	}
	ins, err := h.svc.UpdateBlog(c.Request.Context(), req)
	if err != nil {
		response.Failed(c, err)
		return
	}
	response.Success(c, ins)
}

// 修改博客（全量）： put /vblogs/api/v1/blogs/:id
func (h *blogApiHandler) UpdateBlog(c *gin.Context) {
	req := blog.NewUpdateBlogRequest(c.Param("id"))
	req.UpdateMode = common.UPDATE_MODE_PUT
	// 用户传递的数据
	if err := c.BindJSON(req.CreateBlogRequest); err != nil {
		response.Failed(c, err)
	}
	ins, err := h.svc.UpdateBlog(c.Request.Context(), req)
	if err != nil {
		response.Failed(c, err)
		return
	}
	response.Success(c, ins)

}

// 删除博客： DELETE
func (h *blogApiHandler) DeleteBlog(c *gin.Context) {
	req := blog.NewDeleteBlogRequest(c.Param("id"))
	ins, err := h.svc.DeleteBlog(c.Request.Context(), req)
	if err != nil {
		response.Failed(c, err)
		return
	}
	response.Success(c, ins)
}

// + 查询列表: GET /vblogs/api/v1/blogs?page_size=10&page_number=2
func (h *blogApiHandler) QueryBlog(c *gin.Context) {
	req := blog.NewQueryBlogRequestFromGin(c)
	set, err := h.svc.QueryBlog(c.Request.Context(), req)
	if err != nil {
		response.Failed(c, err)
		return
	}
	response.Success(c, set)
}

// + 查询详情: GET /vblogs/api/v1/blogs/:id
func (h *blogApiHandler) DescribeBlog(c *gin.Context) {
	req := blog.NewDescribeBlogRequest(c.Param("id"))
	ins, err := h.svc.DescribeBlog(c.Request.Context(), req)
	if err != nil {
		response.Failed(c, err)
		return
	}
	response.Success(c, ins)
}
