package impl

import (
	"gitlab.com/golang13-project/go13hui/vblog/apps/blog"
	"gitlab.com/golang13-project/go13hui/vblog/conf"
	"gitlab.com/golang13-project/go13hui/vblog/ioc"
	"gorm.io/gorm"
)

func init() {
	ioc.Controller().Registry(blog.AppName, &blogServiceImpl{})

}

// blog.Service 接口， 是直接注册给Ioc，不需要对我暴露
type blogServiceImpl struct {
	// 依赖一个数据库操作的链接池对象
	db *gorm.DB
}

func (i *blogServiceImpl) Init() error {
	i.db = conf.C().DB()
	return nil
}

func (i *blogServiceImpl) Destory() error {
	return nil
}
