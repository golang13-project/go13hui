package impl_test

import (
	"context"

	"gitlab.com/golang13-project/go13hui/vblog/apps/blog"
	"gitlab.com/golang13-project/go13hui/vblog/ioc"

	// 1. 加载对象
	_ "gitlab.com/golang13-project/go13hui/vblog/apps"
)

// blog service 的实现的具体对象是在ioc中,
// 需要在ioc中获取具体的svc 用来测试

var (
	impl blog.Service
	ctx  = context.Background()
)

func init() {
	// 2. ioc 获取对象
	impl = ioc.Controller().Get(blog.AppName).(blog.Service)

	// ioc 需要初始化 ， 才能填充db属性
	if err := ioc.Init(); err != nil {
		panic(err)
	}
}
