package blog

import (
	"context"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gitlab.com/golang13-project/go13hui/vblog/common"
)

const (
	// 模块名称
	AppName = "blogs"
)

var (
	v = validator.New()
)

// Blog Service 接口定义
type Service interface {
	// 创建一个博客
	CreateBlog(context.Context, *CreateBlogRequest) (*Blog, error)
	// 获取博客列表
	QueryBlog(context.Context, *QueryBlogRequest) (*BlogSet, error)
	// 获取博客详情
	DescribeBlog(context.Context, *DescribeBlogRequest) (*Blog, error)
	// 更新博客
	UpdateBlog(context.Context, *UpdateBlogRequest) (*Blog, error)
	// 删除博客
	DeleteBlog(context.Context, *DeleteBlogRequest) (*Blog, error)
	// 文章状态修改, 比如发布
	ChangedBlogStatus(context.Context, *ChangedBlogStatusRequest) (*Blog, error)
	// 文章审核
	AuditBlog(context.Context, *AuditInfo) (*Blog, error)
}

func NewQueryBlogRequest() *QueryBlogRequest {
	return &QueryBlogRequest{
		PageSize:   20,
		PageNumber: 1,
	}
}

func NewQueryBlogRequestFromGin(c *gin.Context) *QueryBlogRequest {
	req := NewQueryBlogRequest()
	ps := c.Query("page_size")
	if ps != "" {
		req.PageSize, _ = strconv.Atoi(ps)
	}
	pn := c.Query("PageNumber")
	if pn != "" {
		req.PageNumber, _ = strconv.Atoi(pn)
	}
	return req

}

type QueryBlogRequest struct {
	PageSize   int //分页大小
	PageNumber int // 当前页，查询哪一页的数据
}

func (req *QueryBlogRequest) Limit() int {
	return req.PageSize
}

func (req *QueryBlogRequest) Offset() int {
	return req.PageSize * (req.PageNumber - 1)
}
func NewDescribeBlogRequest(id string) *DescribeBlogRequest {
	return &DescribeBlogRequest{
		Id: id,
	}
}

type DescribeBlogRequest struct {
	Id string
}

func NewUpdateBlogRequest(id string) *UpdateBlogRequest {
	return &UpdateBlogRequest{
		Id:                id,
		UpdateMode:        common.UPDATE_MODE_PUT,
		CreateBlogRequest: NewCreateBlogRequest(),
	}
}

type UpdateBlogRequest struct {
	// 被更新的博客 Id
	Id string `json:"id"`
	// 更新模式

	UpdateMode common.UpdateMode `json:"update_mode"`
	// 更新时的数据
	*CreateBlogRequest
}

func NewDeleteBlogRequest(id string) *DeleteBlogRequest {
	return &DeleteBlogRequest{
		Id: id,
	}
}

type DeleteBlogRequest struct {
	Id string
}
