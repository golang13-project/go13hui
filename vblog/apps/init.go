package apps

// 注册业务实现: API + Controller
import (
	// 通过import方法 完成注册
	_ "gitlab.com/golang13-project/go13hui/vblog/apps/blog/api"
	_ "gitlab.com/golang13-project/go13hui/vblog/apps/blog/impl"
	_ "gitlab.com/golang13-project/go13hui/vblog/apps/token/api"
	_ "gitlab.com/golang13-project/go13hui/vblog/apps/token/impl"
	_ "gitlab.com/golang13-project/go13hui/vblog/apps/user/impl"
)
