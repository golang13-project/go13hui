package impl

import (
	"context"

	"gitlab.com/golang13-project/go13hui/vblog/apps/token"
)

// 需要复用的数据操作
func (i *TokenServiceImpl) getToken(ctx context.Context, accessToken string) (*token.Token, error) {
	tk := token.NewToken(false)
	err := i.db.
		WithContext(ctx).
		Model(&token.Token{}).
		Where("access_token = ?", accessToken).
		First(tk).
		Error
	if err != nil {
		return nil, err
	}
	return tk, nil
}
