package impl

import (
	"gitlab.com/golang13-project/go13hui/vblog/apps/token"
	"gitlab.com/golang13-project/go13hui/vblog/apps/user"
	"gitlab.com/golang13-project/go13hui/vblog/conf"
	"gitlab.com/golang13-project/go13hui/vblog/ioc"
	"gorm.io/gorm"
)

var (
	_ token.Service = (*TokenServiceImpl)(nil)
)

func init() {
	ioc.Controller().Registry(token.AppName, &TokenServiceImpl{})
}
func NewTokenServiceImpl(userServiceImpl user.Service) *TokenServiceImpl {
	return &TokenServiceImpl{
		// 获取全局的DB对象
		// 前提： 配置对象准备完成
		db:   conf.C().DB(),
		user: userServiceImpl,
	}
}

//怎么实现token。Service 接口？
// 定义tokenServiceImpl来实现接口

type TokenServiceImpl struct {
	// 依赖了一个数据库操作的链接池对象
	db *gorm.DB

	//依赖user.Service
	// 依赖接口， 不要接口的具体实现
	user user.Service
}

//对象属性初始化
func (i *TokenServiceImpl) Init() error {
	i.db = conf.C().DB()
	i.user = ioc.Controller().Get(user.AppName).(user.Service)
	return nil
}

func (i *TokenServiceImpl) Destory() error {
	return nil
}
