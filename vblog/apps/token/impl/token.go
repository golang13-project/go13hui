package impl

import (
	"context"
	"fmt"

	"gitlab.com/golang13-project/go13hui/vblog/apps/token"
	"gitlab.com/golang13-project/go13hui/vblog/apps/user"
	"gitlab.com/golang13-project/go13hui/vblog/exception"
)

// 登录： 颁发令牌
// 依赖User模块来校验  用户密码是否正确
func (i *TokenServiceImpl) IssueToken(
	ctx context.Context,
	in *token.IssueTokenRequest) (
	*token.Token, error) {
	// 1. 确认用户密码是否正确
	req := user.NewQueryUserRequest()
	req.Username = in.Username
	us, err := i.user.QueryUser(ctx, req)
	if err != nil {
		return nil, err
	}
	if len(us.Items) == 0 {
		return nil, fmt.Errorf("用户名密码错误")
	}
	//校验密码是否正确
	u := us.Items[0]
	if err := u.CheckPassword(in.Password); err != nil {
		return nil, err
	}

	// 2 正确的请求下 ，就颁发用户令牌
	tk := token.NewToken(in.RemindMe)
	// 关联用户信息
	tk.UserId = fmt.Sprintf("%d", u.Id)
	tk.UserName = u.Username
	tk.Role = u.Role

	// 3. 保持token
	err = i.db.WithContext(ctx).
		Model(&token.Token{}).
		Create(tk).
		Error
	if err != nil {
		return nil, err
	}
	return tk, nil

}

// 退出: 撤销令牌,把这个令牌删除
//
func (i *TokenServiceImpl) RevokeToken(
	ctx context.Context,
	in *token.RevokeTokenRequest) (
	*token.Token, error) {
	// 查询Token
	tk, err := i.getToken(ctx, in.AccessToken)
	if err != nil {
		return nil, err
	}
	// Refresh 确认
	err = tk.CheckRefreshToken(in.RefreshToken)
	if err != nil {
		return nil, err
	}
	// 删除Token
	//// DELETE FROM `tokens` WHERE access_token = 'cmh62ncbajf1m8ddlpa0' AND refresh_token = 'cmh62ncbajf1m8ddlpag'
	err = i.db.WithContext(ctx).
		Where("access_token = ?", in.AccessToken).
		Where("refresh_token = ?", in.RefreshToken).
		Delete(&token.Token{}).
		Error
	if err != nil {
		return nil, err
	}
	return tk, nil
}

// 校验令牌
func (i *TokenServiceImpl) ValidateToken(
	ctx context.Context,
	in *token.ValidateTokenRequest) (
	*token.Token, error) {

	if in.AccessToken == "" {
		return nil, exception.ErrUnauthorized
	}
	// 1. 查询Token， 判断令牌是否存在
	tk, err := i.getToken(ctx, in.AccessToken)
	if err != nil {
		return nil, err
	}
	//2.判断令牌是否过期
	if err := tk.ValidateExpired(); err != nil {
		return nil, err
	}
	// 3. 合法令牌返回
	return tk, nil
}
