package impl

import (
	"gitlab.com/golang13-project/go13hui/vblog/apps/user"
	"gitlab.com/golang13-project/go13hui/vblog/conf"
	"gitlab.com/golang13-project/go13hui/vblog/ioc"
	"gorm.io/gorm"
)

func init() {
	ioc.Controller().Registry(user.AppName, &UserServiceImpl{})
}

func NewUserServiceImpl() *UserServiceImpl {
	return &UserServiceImpl{
		//获取全局的DB对象
		//前提： 配置对象准备完成
		db: conf.C().DB(),
	}
}

// 怎么实现user.Service接口?
// 定义UserServiceImpl来实现接口
type UserServiceImpl struct {
	// 依赖了一个数据库操作的链接池对象
	db *gorm.DB
}

func (i *UserServiceImpl) Init() error {
	i.db = conf.C().DB()
	return nil
}

func (i *UserServiceImpl) Destory() error {
	return nil
}
