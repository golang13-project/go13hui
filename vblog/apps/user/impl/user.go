package impl

import (
	"context"

	"gitlab.com/golang13-project/go13hui/vblog/apps/user"
)

// 实现 user.Service
// 怎么判断这个服务有没有实现这个接口呢？
// &UserServiceImpl{} 是会分配内存, 怎么才能不分配内存
// nil 如何声明 *UserServiceImpl 的 nil
// (*UserServiceImpl)(nil) ---> int8 1  int32(1)  (int32)(1)
// nil 就是一个*UserServiceImpl的空指针
var _ user.Service = (*UserServiceImpl)(nil)

// 用户创建
func (i *UserServiceImpl) CreateUser(
	ctx context.Context,
	in *user.CreateUserRequest) (
	*user.User, error) {
	// 1. 校验用户参数请求
	if err := in.Validate(); err != nil {
		return nil, err
	}

	//2. 创建用户实例对象
	u := user.NewUser(in)

	// 3. 把对象持久化（放到数据库里）
	// orm： orm需要定义这个对象 存在那个表里， 以及struct和数据库里表字段的映射关系
	// object ---> row
	//INSERT INTO `users` (`created_at`,`updated_at`,`username`,`password`,`role`,`label`) VALUES (1705129055,1705129055,'admin','123456','admin','{}') RETURNING `id`
	if err := i.db.Create(u).Error; err != nil {
		return nil, err
	}

	// 4. 返回创建好的对象
	return u, nil
}

// 查询用户列表, 对象列表 [{}]
func (i *UserServiceImpl) QueryUser(
	ctx context.Context,
	in *user.QueryUserRequest) (
	*user.UserSet, error) {
	//构造一个mysql 条件查询语句， select * from users where ...
	query := i.db.WithContext(ctx).Model(&user.User{})

	// 构造条件where username = ""
	if in.Username != "" {
		// query 会生成一个新的语句，不会修改query对象本身
		query = query.Where("username = ?", in.Username)
	}
	set := user.NewUserSet()
	// 统计当前有多少个
	// select count(*) from users where ....
	err := query.
		Count(&set.Total).
		Error
	if err != nil {
		return nil, err
	}

	//做真正的分页查需： sql limit offset ， limit
	// limit 20，20 查询的第2页
	// 使用Find 把多行数据查询出来，使用[]User 来接收返回
	err = query.
		Limit(in.Limit()).
		Offset(in.Offset()).
		Find(&set.Items).
		Error
	if err != nil {
		return nil, err
	}
	return set, nil
}

// 查询用户详情, 通过Id查询,
func (i *UserServiceImpl) DescribeUser(
	ctx context.Context,
	in *user.DescribeUserRequest) (
	*user.User, error) {

	query := i.db.WithContext(ctx).Model(&user.User{}).Where("id = ?", in.UserId)

	// 准备一个对象 接受数据库的返回
	u := user.NewUser(user.NewCreateUserRequest())
	if err := query.First(u).Error; err != nil {
		return nil, err
	}
	return u, nil
}
