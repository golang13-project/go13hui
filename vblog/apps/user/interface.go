package user

import (
	"context"

	"github.com/go-playground/validator/v10"
	"github.com/infraboard/mcube/tools/pretty"
	"golang.org/x/crypto/bcrypt"
)

//user.Service, 设计你这个模块提供的接口
//接口定义，一定要考虑兼容性，接口的参数不能变
const (
	AppName = "users"
)

var (
	v = validator.New()
)

type Service interface {
	//用户创建
	CreateUser(context.Context, *CreateUserRequest) (*User, error)

	// 查询用户列表, 对象列表 [{}]
	QueryUser(context.Context, *QueryUserRequest) (*UserSet, error)
	// 查询用户详情, 通过Id查询,
	DescribeUser(context.Context, *DescribeUserRequest) (*User, error)

	// 作业:
	// 用户修改
	// 用户删除

}

//为了避免对象内部出现空指针，指针对象为初始化，为该对象提供一个构造函数
// 还能做一些相关兼容，补充默认值的功能，New+对象名称()

func NewCreateUserRequest() *CreateUserRequest {
	return &CreateUserRequest{
		Role:  ROLE_MEMBER,
		Label: map[string]string{},
	}
}

//用户创建参数
type CreateUserRequest struct {
	Username string `json:"username" validate:"required" gorm:"column:username"`
	Password string `json:"password" validate:"required" gorm:"column:password"`
	Role     Role   `json: "role" validate:"required" gorm:"column:role"`
	//https://gorm.io/docs/serializer.html
	Label map[string]string `json:"label" gorm:"column:label;serializer:json"`
}

func (c *CreateUserRequest) HashPassword() {
	hp, err := bcrypt.GenerateFromPassword([]byte(c.Password), bcrypt.DefaultCost)
	if err != nil {
		return
	}
	c.Password = string(hp)
}

func (c *CreateUserRequest) CheckPassword(pass string) error {
	return bcrypt.CompareHashAndPassword([]byte(c.Password), []byte(pass))
}

func (req *CreateUserRequest) Validate() error {
	// if req.Username == "" {
	// 	return fmt.Errorf("username required")
	// }
	// if req.Password == "" {
	// 	return fmt.Errorf("password required")
	// }

	// validator库, validator.New() 校验器对象
	return v.Struct(req)

}

func NewQueryUserRequest() *QueryUserRequest {
	return &QueryUserRequest{
		PageSize:   20,
		PageNumber: 1,
	}
}

//查询用户列表
type QueryUserRequest struct {
	// 分页大小, 一共多少个
	PageSize int
	// 当前页, 查询哪一页的数据
	PageNumber int
	// 增加用户name查找用户
	Username string
}

// 分页当前页
func (req *QueryUserRequest) Limit() int {
	return req.PageSize
}

func (req *QueryUserRequest) Offset() int {
	return req.PageSize * (req.PageNumber - 1)
}

func NewUserSet() *UserSet {
	return &UserSet{
		Items: []*User{},
	}
}

type UserSet struct {
	// 总共有多少个
	Total int64
	// 当前查询的数据清单
	Items []*User
}

func (u *UserSet) String() string {
	return pretty.ToJSON(u)
}

func NewDescribeUserRequest(uid int) *DescribeUserRequest {
	return &DescribeUserRequest{
		UserId: uid,
	}
}

type DescribeUserRequest struct {
	UserId int
}
