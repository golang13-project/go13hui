package user_test

import (
	"testing"

	"gitlab.com/golang13-project/go13hui/vblog/apps/user"
)

// https://gitee.com/infraboard/go-course/blob/master/day09/go-hash.md#bcrypt
func TestHashPassword(t *testing.T) {
	req := user.NewCreateUserRequest()
	req.Password = "123456"
	u := user.NewUser(req)
	t.Log(u.Password)

	t.Log(u.CheckPassword("123456"))
}
