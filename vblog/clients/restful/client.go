package resful

import (
	"context"

	"github.com/infraboard/mcube/client/rest"
	"gitlab.com/golang13-project/go13hui/vblog/apps/blog"
	"gitlab.com/golang13-project/go13hui/vblog/apps/token"
)

// 服务接口 （ServiceInterface）
func ListBlog(ctx context.Context, in *blog.QueryBlogRequest) (*blog.BlogSet, error) {

	// network (http)
	// JSON

	// req := http.NewRequest("GET", "http://localhost:8010/vblog/api/v1/blogs", OJB)
	// resp := http.DefaultClient.Do(req)

	// io.ReadAll(resp)
	// json.Unmarshal()
	// return resp
	return nil, nil
}

func NewClient(server, username, password string) *Client {
	client := &Client{
		Server: server}
	return client
}

type Client struct {
	Server string
	Token  *token.Token
	rest   *rest.RESTClient
}

func (c *Client) login(username, password string) error {
	// 调用  /login
	return nil
}
