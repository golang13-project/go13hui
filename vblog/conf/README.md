### 程序配置对象
## 配置定义

```
type Config struct {
	MySQL *MySQL
}

// db对象也是一个单列模式
type MySQL struct {
	Host     string `json:"host" yaml:"host" toml:"host" env:"DATASOURCE_HOST"`
	Port     int    `json:"port" yaml:"port" toml:"port" env:"DATASOURCE_PORT"`
	DB       string `json:"database" yaml:"database" toml:"database" env:"DATASOURCE_DB"`
	Username string `json:"username" yaml:"username" toml:"username" env:"DATASOURCE_USERNAME"`
	Password string `json:"password" yaml:"password" toml:"password" env:"DATASOURCE_PASSWORD"`
	Debug    bool   `json:"debug" yaml:"debug" toml:"debug" env:"DATASOURCE_DEBUG"`

	// 判断这个私有属性, 来判断是否返回已有的对象
	db *gorm.DB
	l  sync.Mutex
}


```

## 配置加载
+ 文件：TOML为例
  JSON/YAML/TOML：都有对应的格式的解析库
  ```
  # {"mysql": {"host":...}}
[mysql]
  host = "127.0.0.1"
  port = 3306
  database = ""
  username = ""
  password = ""
  debug = false

  ```

+ 找到一个toml格式的解析库:
```
// github.com/BurntSushi/toml go里使用比较广泛的toml格式解析库
// https://github.com/BurntSushi/toml 查看该库的基本用法
// object <---> toml 配置文件

// % go install github.com/BurntSushi/toml/cmd/tomlv@latest
// % tomlv some-toml-file.toml

func LoadFromFile(filepath string) error {
	c := &Config{}
	if _, err := toml.DecodeFile(filepath, c); err != nil {
		return err
	}
	config = c
	return nil
}

```

++  环境变量
```
DATASOURCE_HOST="127.0.0.1"
DATASOURCE_PORT=3306
DATASOURCE_DB=""
DATASOURCE_USERNAME=""
DATASOURCE_PASSWORD=""

```
os.Getenv("DATASOURCE_HOST")

```
// "github.com/caarlos0/env/v6" 读取环境变量
// env ---> object
func LoadFromEnv() error {
	c := &Config{}
	// env Tag
	if err := env.Parse(c); err != nil {
		return err
	}
	config = c
	// c.MySQL.Host = os.Getenv("DATASOURCE_HOST")
	return nil
}
```

## 程序的默认值
提供默认值 ，让程序基本能跑（开发环境能跑）
```
func DefaultConfig() *Config {
	return &Config{
		MySQL: &MySQL{
			Host:     "127.0.0.1",
			Port:     3306,
			DB:       "vblog",
			Username: "root",
			Password: "123456",
			Debug:    true,
		},
	}
}

```