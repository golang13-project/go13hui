package conf

import (
	"fmt"

	"github.com/BurntSushi/toml"
	"github.com/caarlos0/env/v6"
)

//该文件定义  配置对象的加载方法

// github.com/BurntSushi/toml go里使用比较广泛的toml格式解析库
// https://github.com/BurntSushi/toml 查看该库的基本用法

// object  <---> toml 配置文件

func LoadFromFile(filepath string) error {
	c := DefaultConfig()
	if _, err := toml.DecodeFile(filepath, c); err != nil {
		return err
	}
	config = c
	fmt.Printf("%T,%#[1]v\n", config)
	return nil
}

// "github.com/caarlos0/env/v6" 读取环境变量
// env ---> object

func LoadFromEnv() error {
	c := DefaultConfig()
	fmt.Printf("c:%v", c)
	if err := env.Parse(c); err != nil {
		return err
	}
	config = c
	fmt.Printf("%v", config)
	// c.MySQL.Host = os.Getenv("DATASOURCE_HOST")
	return nil
}
