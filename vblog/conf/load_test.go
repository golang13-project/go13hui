package conf_test

import (
	"testing"

	"gitlab.com/golang13-project/go13hui/vblog/conf"
)

// 测试配置文件
func TestLoadFromFile(t *testing.T) {
	err := conf.LoadFromFile("etc/application.toml")
	if err != nil {
		t.Fatal(err)
	}
	t.Log(conf.C().MySQL)
}

//读取环境变量
// env --- > object

func TestLoadFromEnv(t *testing.T) {
	err := conf.LoadFromEnv()
	if err != nil {
		t.Fatal(err)
	}
	t.Log(conf.C())
}
