package ioc_test

import (
	"fmt"
	"testing"

	"gitlab.com/golang13-project/go13hui/vblog/ioc"
)

func TestContainerGetAndRegistry(t *testing.T) {
	c := ioc.NewContainer()
	c.Registry("TestStruct", &TestStruct{})
	t.Log(c.Get("TestStruct"))

	//断言使用
	c.Get("TestStruct").(*TestStruct).XXX()
}

type TestStruct struct {
}

func (t *TestStruct) Init() error {
	return nil
}

func (t *TestStruct) Destory() error {
	return nil
}

func (t *TestStruct) XXX() error {
	fmt.Println("xxx log")
	return nil
}
