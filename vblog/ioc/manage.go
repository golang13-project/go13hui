package ioc

import "github.com/gin-gonic/gin"

var nc = &NamespaceContainer{
	ns: map[string]*Container{
		"api":        NewContainer(),
		"controller": NewContainer(),
		"config":     NewContainer(),
		"default":    NewContainer(),
	},
}

func Init() error {
	return nc.Init()
}
func Destory() error {
	return nc.Destory()
}

//ioc.Controller().Restriy()
// ioc.Controller().Get()

func Controller() *Container {
	return nc.ns["controller"]
}

// ioc.Api().restriy()
// ioc.Api().Get()
func Api() *Container {
	return nc.ns["api"]
}

func RegistryGinApi(rr gin.IRouter) {
	nc.RegistryGinApi(rr)
}

// 基于这个构建度空间的container
type NamespaceContainer struct {
	// {}/[]
	ns map[string]*Container
}

func (c *NamespaceContainer) Init() error {
	// 遍历Namespace
	for key := range c.ns {
		c := c.ns[key]
		// 遍历Namespace里面的对象
		for objectName := range c.storage {
			if err := c.storage[objectName].Init(); err != nil {
				return err
			}
		}
	}
	return nil
}

func (c *NamespaceContainer) Destory() error {
	// 遍历Namespace
	for key := range c.ns {
		c := c.ns[key]
		for objectName := range c.storage {
			if err := c.storage[objectName].Destory(); err != nil {
				return err
			}
		}

	}
	return nil
}

type GinApi interface {
	//基础约束
	Object
	//额外约束
	//ioc.Api().Get(token.AppName).(*api.TokenApiHandler).Registry(rr)
	Registry(rr gin.IRouter)
}

// 完成模块api注册给gin root router
func (c *NamespaceContainer) RegistryGinApi(rr gin.IRouter) {
	for key := range c.ns {
		c := c.ns[key]
		for objectName := range c.storage {
			obj := c.storage[objectName]
			// 如果判断一个对象是不是GinApi对象(约束)
			// 判断对象有没有Registry(rr gin.IRouter)
			// 断言该对象 满足GinApi接口, 实现了Registry函数
			if v, ok := obj.(GinApi); ok {
				v.Registry(rr)
			}
		}

	}
}
