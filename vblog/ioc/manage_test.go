package ioc_test

import (
	"testing"

	"gitlab.com/golang13-project/go13hui/vblog/apps/token"
	"gitlab.com/golang13-project/go13hui/vblog/ioc"
)

func TestManageGetAndRegistry(t *testing.T) {
	ioc.Controller().Registry(token.AppName, &TestStruct{})
	t.Log(ioc.Controller().Get(token.AppName))

	//断言使用
	ioc.Controller().Get(token.AppName).(*TestStruct).XXX()
}
