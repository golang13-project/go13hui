package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/golang13-project/go13hui/vblog/conf"
	"gitlab.com/golang13-project/go13hui/vblog/ioc"

	// 通过import方法 完成注册
	// _ "gitlab.com/golang13-project/go13hui/vblog/apps/token/api"
	// _ "gitlab.com/golang13-project/go13hui/vblog/apps/token/impl"
	// _ "gitlab.com/golang13-project/go13hui/vblog/apps/user/impl"
	_ "gitlab.com/golang13-project/go13hui/vblog/apps"
)

func main() {
	// // user service impl
	// usvc := user_impl.NewUserServiceImpl()

	// // token service impl
	// tsvc := token_impl.NewTokenServiceImpl(usvc)

	// // api
	// TokenApiHandler := api.NewTokenApiHandler(tsvc)

	// 1.初始化程序配置，这里没有配置，使用默认值
	if err := conf.LoadFromEnv(); err != nil {
		panic(err)
	}
	// 2.程序对象管理
	if err := ioc.Init(); err != nil {
		panic(err)
	}

	// Protocol
	engine := gin.Default()
	rr := engine.Group("/vblog/api/v1")
	// TokenApiHandler.Registry(rr)
	ioc.RegistryGinApi(rr)

	// 把http协议服务器启动起来
	if err := engine.Run(":8080"); err != nil {
		panic(err)
	}
}
