package middleware

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/golang13-project/go13hui/vblog/apps/token"
	"gitlab.com/golang13-project/go13hui/vblog/ioc"
	"gitlab.com/golang13-project/go13hui/vblog/response"
)

func Auth(c *gin.Context) {
	svc := ioc.Controller().Get(token.AppName).(token.Service)

	ak := token.GetAccessTokenFromHttp(c.Request)
	req := token.NewValidateTokenRequest(ak)
	tk, err := svc.ValidateToken(c.Request.Context(), req)
	if err != nil {
		response.Failed(c, err)
		return
	}
	c.Set(token.TOKEN_MIDDLEWARE_KEY, tk)

	// 后面请求如何获取 中间信息
	// c.Get(token.TOKEN_MIDDLEWARE_KEY).(*token.Token).UserName

}
