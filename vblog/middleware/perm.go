package middleware

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"gitlab.com/golang13-project/go13hui/vblog/apps/token"
	"gitlab.com/golang13-project/go13hui/vblog/apps/user"
	"gitlab.com/golang13-project/go13hui/vblog/exception"
	"gitlab.com/golang13-project/go13hui/vblog/response"
)

// Required(user.ROLE_ADMIN) --- gin.HandlerFunc

func Required(roles ...user.Role) gin.HandlerFunc {
	return func(c *gin.Context) {
		//校验用户的角色
		// 直接通过上下文取出Token， 通过Token获取用户角色，来定义访问这个接口的角色
		// 后面请求如何获取 中间信息
		if v, ok := c.Get(token.TOKEN_MIDDLEWARE_KEY); ok {
			//遍历判断  用户是否在运行的角色列表里面
			hasPerm := false
			for _, r := range roles {
				fmt.Println(roles, v.(*token.Token).Role)
				if r == v.(*token.Token).Role {
					hasPerm = true
				}
			}
			if !hasPerm {
				response.Failed(c, exception.ErrPermissionDeny.WithMessagef("允许访问的角色: %v", roles))
			}
		}
	}
}
