# ui

Vblog 前端UI

## 关于设置淘宝源

```
    npm config set registry http://r.cnpmjs.org
    npm config set registry https://registry.npmmirror.com/

    yrm use tn
```

## 安装ui插件

[]https://arco.design/vue/docs/start

## 布局

几个布局

- 登录页面（/login）
- 后台页面（/backend/xxx）
- 前台页面(/frontend/xxx)

## 登录页面

### 前后端对接

熟悉的API 客户端调用方式(postman) []https://www.axios-http.cn/docs/intro

- post('')
- get('')

```
npm install axios

axios.post('/user', {
    firstName: 'Fred',
    lastName: 'Flintstone'
  })
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });
```

cros中间件： 允许那个网站来访问你的后端API
不跨越 /xxxx.com/api/ ---> backend /xxxx.com/\* ---> frontend
![] ./docs/proxy.drawio

配置vite服务器的代理设置

```json
 server: {
    proxy: {
      // string shorthand: http://localhost:5173/foo -> http://localhost:4567/foo
      // '/foo': 'http://localhost:4567',
      '/vblog/api/v1' : 'http://127.0.0.1:8080'
    }
  }
```

非代理模式(后端处理跨域)：

```js
axios.post('https://127.0.0.1:8080/vblog/api/v1/tokens', form.value)
```

代理模式（前端处理跨域）:

```js
axios.post('/vblog/api/v1/tokens', form.value)
```

### 关于axios

- 临时创建一个http 实例进行请求： axios.pos(), create http client
- 复用一个http

```
  client = create()
  client.get()
```

添加中间件

## 后台

## 前台
