// 封装一个统一的httpclient 实例

import axios from 'axios'
import { Message } from '@arco-design/web-vue';

// 设置实例统一通用配置
var instance = axios.create({
    timeout: 5000,
})

// 为实例添加统一的拦截器
instance.interceptors.response.use(
    // 成功处理
    (response) => { 
        // console.log(response)
        return response.data
    },
    // 请求异常这么处理(!200)
    (err) => { 
        var msg = err.message
        if (err.response.data && err.response.data.message) { 
            msg = err.response.data.message
        }
        Message.error({
            content: msg,
            duration: 2000,
        })
        // console.log(err)
        // return Promise.reject(err)

    }
)
export default instance