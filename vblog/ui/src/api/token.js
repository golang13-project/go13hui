import client from './client'

//封装一个lOGIn api（restful） == 》 LOGIN({})
export const LOGIN = (data) => client({
    url: '/vblog/api/v1/tokens',
    method: 'post',
    data:data,
})