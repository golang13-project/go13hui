import { createRouter, createWebHistory } from 'vue-router'

// import { BackendLayout } from '../views/backend/BackendLayout.vue'
// import { FrontendLayout } from '../views/frontend/FrontendLayout.vue'

const router = createRouter({
  //backend/xx
  // /backend#page1
  //https://cn.vitejs.dev/guide/env-and-mode
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    // {
    //   path: '/',
    //   name: 'home',
    //   component: HomeView
    // },
    {
      path: '/login',
      name: 'LoginView',
      component: () => import('../views/LoginView.vue')
    },

    //     {
    //   path: '/backend',
    //   name: 'BackendLayout',
    //   component: BackendLayout,
    // },

    //     {
    //   path: '/frontend',
    //   name: 'FrontendLayout',
    //   component: FrontendLayout,
    // },

  ]
})

export default router

